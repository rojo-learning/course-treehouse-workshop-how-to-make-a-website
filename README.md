Treehouse Workshop - How to Make a Website
===========================================

Project from the «Rails Development» [Treehouse](https://teamtreehouse.com/)'s study track.

The repository includes the following developed works:

* Stage 1: Workshop - How to Make a Website
If you’ve never built a website before and you have no coding or design experience, this is the place to start. In this project, we learn how to build a modern portfolio website for desktops, tablets, and mobile devices.

---
**Note**: The code in this repository was generated by following the step by step instructions of the course, and mostly is the same that the showed in the course material. Therefore, the code belongs to Treehouse and is included here only for showcase purposes.